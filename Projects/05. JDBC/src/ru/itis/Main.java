package ru.itis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

public class Main {

    private static String DB_URL = "jdbc:postgresql://localhost:5432/example_db";
    private static String DB_USERNAME = "postgres";
    private static String DB_PASSWORD = "qwerty007";

    public static void main(String[] args) throws Exception {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);

        UsersRepository usersRepository = new UsersRepositoryImpl(connection);
//        User user1 = usersRepository.find(1L);
//        User user2 = usersRepository.find(2L);
//        System.out.println(user1);
//        System.out.println(user2);
        List<User> users = usersRepository.findAll();
        System.out.println(users);

        connection.close();
    }
}
