package ru.itis;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryImpl implements UsersRepository {

    private Connection connection;

    public UsersRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    // (args) -> { expression }
//    private RowMapper<User> userRowMapper = new RowMapper<User>() {
//        @Override
//        public User mapRow(ResultSet row) {
//            return new User(row.getLong("id"), row.getString("name"),
//                    row.getString("email"),
//                    row.getString("password"));
//        }
//    };

    // (args) -> { expression }
    private RowMapper<User> userRowMapper = row -> {
        return new User(row.getLong("id"), row.getString("name"),
                row.getString("email"),
                row.getString("password"));
    };

    @Override
    public User find(Long id) {
        try {
            PreparedStatement statement =
                    connection.prepareStatement("select * from itis_user where  id = ?");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            User user =  userRowMapper.mapRow(resultSet);
            resultSet.close();
            statement.close();
            return user;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<User> findAll() {
        try {
            PreparedStatement statement =
                    connection.prepareStatement("select * from itis_user");
            List<User> result = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user =  userRowMapper.mapRow(resultSet);
                result.add(user);
            }
            resultSet.close();
            statement.close();
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(User entity) {

    }

    @Override
    public void save(User entity) {

    }

    @Override
    public void update(User entity) {

    }
}
