package ru.itis;

import java.util.List;

public interface CrudRepository<T> {
    T find(Long id);
    List<T> findAll();

    void delete(T entity);
    void save(T entity);
    void update(T entity);
}
