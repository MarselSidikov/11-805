import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        List<String> lines = new ArrayList<>();
        lines.add("Марсель");
        lines.add("Ринат");
        lines.add("Тема");
        lines.add("Булат");
        lines.add("Римма");
        lines.add("Даша");

//        Stream<String> stringsStream = lines.stream();
//
//        Function<String, Integer> lengthFunction = line -> {
//            return line.length();
//        };
//
//        Stream<Integer> integersStream = stringsStream.map(lengthFunction);
//
//        Predicate<Integer> evenOrOddPredicate = number -> {
//            return number % 2 == 0;
//        };
//
//        Stream<Integer> evensIntegersStream = integersStream.filter(evenOrOddPredicate);
//
//        Consumer<Integer> printConsumer = number -> {
//            System.out.println(number);
//        };
//
//        evensIntegersStream.forEach(printConsumer);
        lines.stream()
                .map(String::length)
                .filter(number -> number % 2 == 0)
                .forEach(System.out::println);
    }
}
