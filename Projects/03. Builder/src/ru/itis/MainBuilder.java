package ru.itis;

public class MainBuilder {
    public static void main(String[] args) {
        User.Builder builder = new User.Builder();
//        User.Builder b2;
//        User.Builder b3;
//        User.Builder b4;
//        User.Builder b5;
//        User.Builder b6;
//
//        b2 = builder.firstName("Марсель");
//        b3 = b2.lastName("Сидиков");
//        b4 = b3.age(26);
//        b5 = b4.height(1.85);
//        b6 = b5.weight(70);
//        builder.firstName("Марсель").lastName("Сидиков").age(26);

//        User user = new User(builder);

        User user = User.builder()
                .firstName("Марсель")
                .lastName("Сидиков")
                .build();
    }
}
