package ru.itis;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private double height;
    private double weight;

//    public User(String firstName, String lastName, int age, double height, double weight) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.age = age;
//        this.height = height;
//        this.weight = weight;
//    }
//
//    public User(String firstName, String lastName, int age, double height) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.age = age;
//        this.height = height;
//    }
//
//    public User(String firstName, String lastName, int age) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.age = age;
//    }

    private User(Builder builder) {
//        if (builder.firstName == null || builder.lastName == null) {
//            throw new IllegalArgumentException();
//        }
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.height = builder.height;
        this.weight = builder.weight;
    }

    public static class Builder {
        private String firstName;
        private String lastName;
        private int age;
        private double height;
        private double weight;

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public Builder height(double height) {
            this.height = height;
            return this;
        }

        public Builder weight(double weight) {
            this.weight = weight;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    public static Builder builder() {
        return new Builder();
    }
}
