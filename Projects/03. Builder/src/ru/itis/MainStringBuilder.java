package ru.itis;

import java.util.Random;

public class MainStringBuilder {

    public static void main(String[] args) {
        String text = "Hello";
        text = text + ", Marsel";

        Random random = new Random();

        String randoms = "";
        for (int i = 0; i < 1000; i++) {
            randoms += random.nextInt();
        }

        System.out.println(randoms);

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < 1000; i++) {
            builder.append(random.nextInt());
        }

        randoms = builder.toString();

        builder.append("Hello")
                .append(", Marsel")
                .append(", Bye!");

    }
}
