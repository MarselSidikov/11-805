package ru.itis.springbootdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.springbootdemo.dto.ProductsSearchResult;
import ru.itis.springbootdemo.service.SearchService;

@RestController
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @GetMapping("/products")
    public ProductsSearchResult searchProducts(@RequestParam("q") String query,
                                               @RequestParam(value = "available", required = false) Boolean available,
                                               @RequestParam("page") Integer page,
                                               @RequestParam("size") Integer size) {
        return searchService.searchProducts(query, available, page, size);
    }
}
