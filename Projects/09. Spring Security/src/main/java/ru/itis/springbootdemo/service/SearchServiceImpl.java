package ru.itis.springbootdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.itis.springbootdemo.dto.ProductsSearchResult;
import ru.itis.springbootdemo.models.Product;
import ru.itis.springbootdemo.repositories.ProductsRepository;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private ProductsRepository productsRepository;

    @Override
    public ProductsSearchResult searchProducts(String query, Boolean available, Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page, size);
        Page<Product> pageResult = productsRepository.search(query, available, pageRequest);
        return ProductsSearchResult.builder()
                .pagesCount(pageResult.getTotalPages())
                .products(pageResult.getContent())
                .build();
    }
}
