package ru.itis.springbootdemo.service;

import ru.itis.springbootdemo.dto.ProductsSearchResult;

public interface SearchService {
    ProductsSearchResult searchProducts(String query, Boolean available, Integer page, Integer size);
}
