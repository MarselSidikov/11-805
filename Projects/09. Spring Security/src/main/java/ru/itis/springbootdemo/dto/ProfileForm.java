package ru.itis.springbootdemo.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class ProfileForm {
    @Email(message = "{errors.incorrect.email}")
    private String email;

    @NotNull(message = "{errors.null.age}")
    @Min(value = 0, message = "{errors.incorrect.age}")
    private Integer age;

//    @Pattern(regexp = "^id*$")
//    @NotBlank
//    private String vkId;
}
