package ru.itis.springbootdemo.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import ru.itis.springbootdemo.dto.SmsText;

import java.net.URI;

@RestController
@RequestMapping("/api/sms-management/")
@Slf4j
public class SmsApiController {
    // TODO: сделать сервис

    @Autowired
    private RestTemplate restTemplate;

    // https://email:api_key@gate.smsaero.ru/v2/sms/send?number=79000000000&text=youtext&sign=SMS Aero&channel=DIRECT

    @PostMapping("/sms/send")
    public String sendSms(@RequestParam("phone") String phone, @RequestBody SmsText text) {
        String uri = "https://gate.smsaero.ru" +
                "/v2/sms/send?number=" + phone + "&text=" + text.getText() + "&channel=DIRECT&sign=SMS%20Aero";
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic c2lkaWtvdi5tYXJzZWxAZ21haWwuY29tOjJOSGVTcXVkbUNxWTRQNUZFajdRMkREcXNSZEg=");
        RequestEntity<Void> requestEntity = new RequestEntity<>(headers,HttpMethod.GET, URI.create(uri));
        return restTemplate.exchange(requestEntity, String.class).getBody();
    }
}
