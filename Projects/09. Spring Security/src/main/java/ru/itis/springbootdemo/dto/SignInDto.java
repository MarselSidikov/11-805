package ru.itis.springbootdemo.dto;

import lombok.Data;

@Data
public class SignInDto {
    private String email;
    private String password;
}
