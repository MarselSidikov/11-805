package ru.itis.springbootdemo.service;

import ru.itis.springbootdemo.dto.SignInDto;

import java.util.Optional;

public interface SignInService {
    Optional<String> signIn(SignInDto signInDto);
}
