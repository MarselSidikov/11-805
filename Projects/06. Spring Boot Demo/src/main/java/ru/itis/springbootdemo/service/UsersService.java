package ru.itis.springbootdemo.service;

import ru.itis.springbootdemo.dto.SignUpDto;
import ru.itis.springbootdemo.dto.UserDto;

import java.util.List;

public interface UsersService {
    List<UserDto> getAllUsers();

    UserDto getUser(Long userId);
}
