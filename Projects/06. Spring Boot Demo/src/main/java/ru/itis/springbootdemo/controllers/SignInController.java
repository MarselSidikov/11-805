package ru.itis.springbootdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itis.springbootdemo.dto.SignInDto;
import ru.itis.springbootdemo.service.SignInService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Controller
@RequestMapping("/signIn")
public class SignInController {
    @Autowired
    private SignInService signInService;

    @GetMapping
    public String getSignPage() {
        // TODO: если пользователь уже авторизован, его нужно кидать на /users
        return "sign_in";
    }

    @PostMapping
    public String signIn(SignInDto signInDto, HttpServletResponse response, Model model) {
        Optional<String> cookieCandidate = signInService.signIn(signInDto);
        if (cookieCandidate.isPresent()) {
            Cookie cookie = new Cookie("AUTH", cookieCandidate.get());
            response.addCookie(cookie);
            return "redirect:/users";
        } else {
            model.addAttribute("isNotAuthorized", new Object());
            return "sign_in";
        }
    }
}
