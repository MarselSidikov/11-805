package ru.itis.springbootdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.springbootdemo.dto.SignInDto;
import ru.itis.springbootdemo.models.CookieValue;
import ru.itis.springbootdemo.models.User;
import ru.itis.springbootdemo.repositories.CookieValuesRepository;
import ru.itis.springbootdemo.repositories.UsersRepository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class SignInServiceImpl implements SignInService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private CookieValuesRepository cookieValuesRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Optional<String> signIn(SignInDto signInDto) {
        Optional<User> userOptional = usersRepository.findByEmail(signInDto.getEmail());
        String rawPassword = signInDto.getPassword();
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            String hashPassword = user.getHashPassword();
            if (passwordEncoder.matches(rawPassword, hashPassword)) {
                String newCookie = UUID.randomUUID().toString();

                CookieValue cookieValue = CookieValue.builder()
                        .value(newCookie)
                        .createdAt(LocalDateTime.now())
                        .user(user)
                        .build();

                cookieValuesRepository.save(cookieValue);

                return Optional.of(newCookie);
            } else {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }
}
