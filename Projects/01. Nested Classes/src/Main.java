public class Main {

    public static void main(String[] args) {
//        LinkedList.Node<Integer> node = new LinkedList.Node<>();
        LinkedList<Integer> linkedList = new LinkedList<>();
        Iterator<Integer> integerIterator = linkedList. new LinkedListIterator();

        Logger logger = new Logger() {
            @Override
            public void info(String message) {
                System.out.println(message);
            }

            @Override
            public void error(String message) {
                System.err.println(message);
            }
        };

        logger.info("Марсель");
        logger.error("Сидиков");

        Summator summator = (a, b) -> {
            return a + b;
        };

        System.out.println(summator.sum(10, 5));
    }
}
