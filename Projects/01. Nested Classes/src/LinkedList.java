public class LinkedList<T> {
    private Node<T> first;

    // статический вложенный класс - вложенный (nested)
    private static class Node<V> {
        V value;
        Node<V> next;
    }

    // нестатический вложенный класс - внутренние (inner)
    public class LinkedListIterator implements Iterator<T> {
        private Node<T> current;

        LinkedListIterator() {
            this.current = first;
        }
        @Override
        public T next() {
            T value = current.value;
            current = current.next;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }
    }
}
