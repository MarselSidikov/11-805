public interface Summator {
    int sum(int a, int b);
}
