package ru.itis.resources;

public class CreditCard {
    // сумма
    private int amount;

    public CreditCard(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    // делает покупку на определенную сумму, если
    // успешно - возвращает true, если нет - false
    public boolean buy(int cost) {
        // если сумма меньше той, что лежит на карте
        if (cost <= amount) {
            // делаем покупку
            this.amount -= cost;
            return true;
        } else {
            // если нет, то нет денег
            System.out.println("НЕТ ДЕНЕГ!");
            return false;
        }
    }
}
