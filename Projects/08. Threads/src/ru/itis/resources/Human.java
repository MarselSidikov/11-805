package ru.itis.resources;

// класс-поток, который взаимодействует с кредитной картой
public class Human extends Thread {
    private String name;
    // кредитная карта
    // final - потому что это объект синхронизации
    // на него смотрят потоки и понимают, можно ли дальше работать или нет
    // в данном случае card - мьютекс
    // мьютекс - одноместный семафор, он разрешает выполнение
    // кода только одному потоку
    // мьютексом может быть любой объект
    // поток ставит специальный флаг в объекте этого класса
    // и другие потоки не могут ничего сделать, пока
    // этот флажок стоит
    private final CreditCard card;

    public Human(String name, CreditCard card) {
        this.name = name;
        this.card = card;
    }

    @Override
    public void run() {
        // идет 1000 покупок
        for (int i = 0; i < 1000; i++) {
            // поток занимает мьютекс и пока он его не освободит
            // никто ничего не может делать
            synchronized (card) {
                if (card.getAmount() > 0) {
                    System.out.println(name + " покупает");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        throw new IllegalArgumentException(e);
                    }
                    if (card.buy(10)) {
                        System.out.println(name + " купил");
                    } else {
                        System.out.println(name + " говорит ЭЭЭЭЭЭ");
                    }
                }
            }
        }
    }
}
