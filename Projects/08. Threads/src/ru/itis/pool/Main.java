package ru.itis.pool;

public class Main {
    public static void main(String[] args) {
        ThreadPool threadPool = new ThreadPool(3);
        threadPool.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("A " + Thread.currentThread().getName());
            }
        });
        threadPool.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("B " + Thread.currentThread().getName());
            }
        });
        threadPool.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("C " + Thread.currentThread().getName());
            }
        });
    }
}
