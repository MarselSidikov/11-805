package ru.itis.pool;

import java.util.Deque;
import java.util.LinkedList;

public class ThreadPool {
    // очередь задач
    private final Deque<Runnable> tasks;
    // массив потоков
    private PoolWorker threads[];
    // когда создаем пул потоков указываем сколько потоков
    // должно работать одновременно
    public ThreadPool(int threadsCount) {
        // создаем очередь задач на основе связного списка
        this.tasks = new LinkedList<>();
        // создаем массив потоков
        this.threads = new PoolWorker[threadsCount];
        for (int i = 0; i < threads.length; i++) {
            // создаем каждый поток
            this.threads[i] = new PoolWorker();
            // сразу его запускаем
            this.threads[i].start();
        }
    }

    public void submit(Runnable task) {
        // когда кладем задачу в очередь
        // мы эту очередь блокируем
        synchronized (tasks) {
            // кладем задачу в очередь
            tasks.add(task);
            // оповещаем остальные потоки о том
            // что здесь есть новая задача
            tasks.notify();
        }
    }
    // класс, описывающий поток ThreadPool-а
    private class PoolWorker extends Thread {
        @Override
        public void run() {
            // создаем объектную переменную для задачи
            Runnable task;
            // бесконечный цикл, каждый поток выполняется бесконечно
            // и не завершается
            while (true) {
                // блокируем очередь задач
                synchronized (tasks) {
                    // пока задач нет
                    while (tasks.isEmpty()){
                        try {
                            // уходим в ожидание
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalArgumentException(e);
                        }
                    }
                    // вытаскиваем задачу
                    task = tasks.pollFirst();
                }
                // выполняем задачу
                task.run();
            }
        }
    }
}
