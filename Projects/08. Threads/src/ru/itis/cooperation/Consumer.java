package ru.itis.cooperation;

// поток потребитель
public class Consumer extends Thread {
    // у нее есть продукт, который можно потреблять
    private final Product product;

    public Consumer(Product product) {
        this.product = product;
    }

    @Override
    public void run() {
        // бесконечный цикл
        while (true) {
            // блокируем продукт
            synchronized (product) {
                // пока продукт не готов
                while (!product.isProduced()) {
                    System.out.println("Consumer ждет");
                    try {
                        // уходим в ожидание
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalArgumentException();
                    }
                }
                // если продукт был подготовлен
                System.out.println("Cosumer потребил");
                // потребляем
                product.consume();
                // оповещаем потоки, которые ушли в ожидание
                // о том, что продукт потребили
                product.notify();
            }
        }
    }
}
