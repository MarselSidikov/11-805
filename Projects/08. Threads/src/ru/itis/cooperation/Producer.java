package ru.itis.cooperation;

public class Producer extends Thread {
    // продукт, который можно подготовить
    private final Product product;

    public Producer(Product product) {
        this.product = product;
    }

    @Override
    public void run() {
        // бесконечный цикл
        while (true) {
            // блокируем продукт
            synchronized (product) {
                // если продукт не потреблен
                while (!product.isConsumed()) {
                    System.out.println("Producer ждет");
                    try {
                        // уходим в ожидание
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
                // если продукт был потреблен мы выходим из цикла
                System.out.println("Producer подготовил");
                // готовим продукт
                product.produce();
                // оповещаем другие потоки о том, что у нас
                // продукт подготовлен
                product.notify();
            }
        }
    }
}

