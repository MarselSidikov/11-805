package ru.itis.cooperation;

// продукт
public class Product {
    // флажок готовности продукта
    private boolean isReady = false;
    // если продукт "готов", значит он "произведен"
    public boolean isProduced() {
        return isReady;
    }
    // если продукт не готов - значит он "потреблен"
    public boolean isConsumed() {
        return !isReady;
    }
    // произвести продукт
    public void produce() {
        this.isReady = true;
    }
    // подтребить продукт
    public void consume() {
        this.isReady = false;
    }
}
