package ru.itis.base;

// сервис для запуска задач в отдельных потоках
public class ThreadsService {
    // принимает на вход задачу в виде Runnable
    public void submit(Runnable task) {
        // создает новый поток
        Thread thread = new Thread(task);
        // запускает
        thread.start();
    }
}
