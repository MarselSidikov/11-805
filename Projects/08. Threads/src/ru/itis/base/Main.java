package ru.itis.base;


public class Main {

    public static void main(String[] args) throws Exception {
//        HenThread henThread = new HenThread();
//        EggThread eggThread = new EggThread();
//        henThread.start();
//        eggThread.start();
//
//        Tirex tirex = new Tirex();
//        Thread tirexThread = new Thread(tirex);
//        tirexThread.start();

        ThreadsService threadsService = new ThreadsService();

        threadsService.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("Hello!");
            }
        });

        threadsService.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("Bye");
            }
        });
    }
}
