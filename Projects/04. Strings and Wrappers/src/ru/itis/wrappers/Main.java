package ru.itis.wrappers;

public class Main {
    public static void main(String[] args) {
        Integer i1 = 5;
        Integer i2 = 5;

        Integer i3 = 10;
        Integer i4 = 10;

        Integer i5 = 120;
        Integer i6 = 120;

        Integer i7 = 130;
        Integer i8 = 130;

        System.out.println(i1 == i2);
        System.out.println(i3 == i4);
        System.out.println(i5 == i6);
        System.out.println(i7 == i8);
    }
}
