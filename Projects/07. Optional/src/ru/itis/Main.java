package ru.itis;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepository();
//        User user = usersRepository.findByName("Artyom");
//        if (user != null) {
//            System.out.println(user.getAge());
//        }

//        Optional<User> userOptional = usersRepository.findByName("Artyom");
//        if (userOptional.isPresent()) {
//            User user = userOptional.get();
//            System.out.println(user.getAge());
//        }
        usersRepository.findByName("Marsel").ifPresent(user -> System.out.println(user.getAge()));
        User user = usersRepository.findByName("Artyom").orElse(new User(33, "Igor"));
    }
}
