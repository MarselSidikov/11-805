package ru.itis;

import java.util.Optional;

public class UsersRepository {
    public Optional<User> findByName(String name) {
        if (name.equals("Marsel")) {
            User result = new  User(26, "Marsel");
            return Optional.of(result);
        } else {
            return Optional.empty();
        }
    }
}
